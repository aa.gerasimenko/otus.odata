﻿using Simple.OData.Client;

var x = ODataDynamic.Expression;
var host = "http://localhost:5102/odata"; //"https://services.odata.org/v4/TripPinServiceRW/"
var client = new ODataClient(host);
var products = await client
    .For(x.Products)
    .Filter(x.Sales.Any(x.Amount > 2))
    .Top(2)
    .Select(x.Name, x.Id)
    .FindEntriesAsync() as IEnumerable<dynamic>;
foreach (var p in products)
    Console.WriteLine($"{p.Id}: {p.Name}");